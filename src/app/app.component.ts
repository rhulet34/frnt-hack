import { Component } from '@angular/core';
declare let $;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  player;
  otherPlayer;
  gameStart = false;
  startOfRound = true;
  isMyTurn = true;
  prompt = this.startOfRound && this.isMyTurn ? 'Choose a Prompt' : 'Wait for a Prompt';
  guessList = [];
  otherPlayersGuessList = [];
  game = $.hubConnection("http://byubhackathon.azurewebsites.net").createHubProxy("movieHub");

  constructor() {
    this.game.connection.broadcastMessage = (name, message) => {
      console.log(`${name}: ${message}`);
    };

    this.game.connection.start()
      .done(() => {
        this.game.connection.send('hello', 'hey');
      })
      .fail(() => console.error('Coudn\'t establish connection.'));

    console.log(this.game.connection);
  }
  updatePrompt(title) {
    this.prompt = title;
    this.startOfRound = false;
    this.isMyTurn = false;
  }

  updateGuessList(title) {
    this.guessList.push({
      title: title,
      isBadGuess: (this.prompt === title || this.guessList.find(guess => guess.title === title) || this.otherPlayersGuessList.find(guess => guess.title === title)) ? true : false
    });
  }

  login(login) {
    this.player = {
      username: login.username,
      password: login.password
    }
    this.gameStart = true;
  }
}
